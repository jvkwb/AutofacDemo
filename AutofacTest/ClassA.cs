﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacTest
{
    public class ClassA
    {
        private readonly ClassB b;

        public ClassA(ClassB b)
        {
            this.b = b;
        }

        public void Show()
        {
            Console.WriteLine("I am ClassA's instance !");
        }
    }

    public class ClassE
    {
        public ClassF F { get; set; }

      

        public void Show()
        {
            Console.WriteLine("I am ClassE's instance !");
        }
    }
    public class ClassF
    {
        public ClassE E { get; set; }

        public void Show()
        {
            Console.WriteLine("I am ClassF's instance !");
        }
    }

    public class ClassB
    {
        public ClassA A { get; set; }

        public void Show()
        {
            Console.WriteLine("I am ClassB's instance !");
        }

    }

    public class ClassC
    {
        public string Name { get; set; }

        public ClassD D { get; set; }

        public void Show()
        {
            Console.WriteLine("I am ClassC's instance !" + Name);
        }
    }

    public class ClassD
    {
        public void Show()
        {
            Console.WriteLine("I am ClassD's instance !");
        }
    }
}
