﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Autofac;
using System.Web.Mvc;
using System.Reflection;
using ClassLibrary;

namespace AutofacTest
{
    /// <summary>
    /// PropertyInjection 的摘要说明
    /// </summary>
    [TestClass]
    public class PropertyInjection
    {
        public PropertyInjection()
        {



        }

        private TestContext testContextInstance;

        /// <summary>
        ///获取或设置测试上下文，该上下文提供
        ///有关当前测试运行及其功能的信息。
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region 附加测试特性
        //
        // 编写测试时，可以使用以下附加特性: 
        //
        // 在运行类中的第一个测试之前使用 ClassInitialize 运行代码
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // 在类中的所有测试都已运行之后使用 ClassCleanup 运行代码
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // 在运行每个测试之前，使用 TestInitialize 来运行代码
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // 在每个测试运行完之后，使用 TestCleanup 来运行代码
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        /// <summary>
        /// 反射组件
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            var builder = new ContainerBuilder();

            builder.RegisterType<ClassD>();
            var s = builder.RegisterType<ClassC>().PropertiesAutowired(); //使用PropertiesAutowired会自动解析在容器中注册了的类型的属性

            var container = builder.Build();


            var c = container.Resolve<ClassC>();
            c.Show();
            c.D.Show();
        }

        /// <summary>
        /// 循环注入
        /// </summary>
        [TestMethod]
        public void TestMethod2()
        {
            var builder = new ContainerBuilder();

            //解决循环依赖方法一
            //builder.RegisterType<ClassB>().PropertiesAutowired(PropertyWiringOptions.AllowCircularDependencies).SingleInstance();
            //builder.Register(n => new ClassA(n.Resolve<ClassB>()));

            //解决循环依赖方法二
            builder.Register(c => new ClassE()).OnActivated(n => n.Instance.F = n.Context.Resolve<ClassF>());
            builder.RegisterType<ClassF>();

            var container = builder.Build();

            //var b = container.Resolve<ClassB>();
            //b.Show();
            //b.A.Show();


            var e = container.Resolve<ClassE>();
            e.Show();
            e.F.Show();

        }


        [TestMethod]
        public void TestMethod3()
        {
            var builder = new ContainerBuilder();
            builder.RegisterAssemblyTypes(Assembly.Load("ClassLibrary")).AsSelf().AsImplementedInterfaces();


            var dataAccess = Assembly.GetExecutingAssembly();

            builder.RegisterAssemblyTypes(dataAccess).PublicOnly();

            var container = builder.Build();


            var e = container.Resolve<InternalClass>();

        }

    }
}
