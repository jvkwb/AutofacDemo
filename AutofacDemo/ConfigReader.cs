﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacDemo
{
    public class ConfigReader:IConfigReader
    {
        private string _section;

        public ConfigReader()
        {
            _section = string.Empty;
        }

        public ConfigReader(string section)
        {
            _section = section;
        }

        public void Read()
        {
            Console.WriteLine("ReadSection:"+ _section);  
        }

    }
}
