﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacDemo
{
    public class NHibernateRepository<T> : IRepository<T>
    {
        public NHibernateRepository()
        {
            Console.WriteLine("NHibernateRepository:" + typeof(T).FullName);
        }
    }
}
