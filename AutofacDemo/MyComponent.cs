﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutofacDemo
{
    public class MyComponent
    {
        public MyComponent()
        {
            Console.WriteLine("无参构造函数");
        }

        public MyComponent(ILogger logger)
        {
            Console.WriteLine("一个参数的构造函数");
        }

        public MyComponent(ILogger logger, IConfigReader reader)
        {
            Console.WriteLine("两个参数的构造函数");
        }
    }
}
